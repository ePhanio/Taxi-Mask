package fr.et.intechinfo.mousqinfos.taximask.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import fr.et.intechinfo.mousqinfos.taximask.models.Marque;

public interface MarqueRepository extends JpaRepository<Marque, Long> {
	
	

}
